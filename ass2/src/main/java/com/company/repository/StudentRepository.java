package com.company.repository;

import com.company.model.Student;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository extends CrudRepository<Student,Long> {

    @Query(value = "SELECT * FROM student WHERE id > 0", nativeQuery = true)
    Student getStudent();

    List<Student> findAllByGroupId(long group_id);

}
